import axios from 'axios';
import store from '@/store/store'
const instance = axios.create();

// instance.defaults.baseURL = 'https://api.example.com';
instance.defaults.baseURL = 'https://apitest.mfu.ac.th/misapi';
// instance.defaults.baseURL = 'http://10.1.4.63/misapi';
instance.defaults.timeout = 5000;

instance.defaults.headers = {
  "Content-Type": "application/json",
  "api-key": "12345678900987654321",
  "api-version": "1.0",
  // "studentcode" : `${store.state.user.username}`,
  // "userid" : `${store.state.user.username}`,
  // "officerid":`${store.state.user.username}`
}


export default {

  onSetToken(){
      instance.defaults.headers = {
          "Content-Type": "application/json",
          "api-key": "12345678900987654321",
          "api-version": "1.0",
          "Authorization": `Bearer ${store.state.token}`,
          "studentcode" : `${store.state.username}`,
          "userid" : `${store.state.username}`,
          "officerid":`${store.state.username}`
      }

  },

  onLogin(body) {
    return instance.post("/authen/APILogin",body)
  },


}
