import Vue from 'vue'
import Router from 'vue-router'

// Containers
const TheContainer = () => import('@/containers/TheContainer')

const Dashboard = () => import('@/views/Dashboard')
const Login = () => import('@/components/Login')

const Blank = () => import('@/components/blank/Blank')

// Views - Pages
const Page404 = () => import('@/components/page/Page404')
const Page500 = () => import('@/components/page/Page500')



Vue.use(Router)

export default new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes () {
  return [

      {
          path: '/',
          redirect: '/login',
          name: 'Pages',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'login',
              name: 'Login',
              component: Login
            }
          ]
      },
      {
          path: '/student',
          redirect: '/student/dashboard',
          name: 'Home',
          component: TheContainer,
          children: [
              {
                  path: 'dashboard',
                  name: 'Dashboard',
                  component: Blank
              }
          ]
      },
      {
          path: '/pages',
          redirect: '/pages/404',
          name: 'Pages',
          component: {
              render (c) { return c('router-view') }
          },
          children: [
              {
                  path: '404',
                  name: 'Page404',
                  component: Page404
              },
              {
                  path: '500',
                  name: 'Page500',
                  component: Page500
              }
          ]
      }
  ]
}

